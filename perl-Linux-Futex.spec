Name:           perl-Linux-Futex
Version:        0.6
Exclusiveos:    linux
ExclusiveArch:  x86_64
Release:        1%{?dist}
Summary:        Perl Interface to Futexes
License:        MIT
Group:          Development/Libraries
URL:            https://github.com/townsen/%{name}
Source0:        https://github.com/townsen/%{name}/archive/%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      x86_64
BuildRequires:  perl(ExtUtils::MakeMaker)
BuildRequires:  perl(Exporter)
BuildRequires:  perl(Test)
BuildRequires:  perl(Test::More)
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%{?perl_default_filter}

%define _unpackaged_files_terminate_build 1

%description
Futex provides an implementation of fast mutexes using the 
Linux kernel Futex capability.

%prep
%{__rm} -rf $RPM_BUILD_DIR/%{name}-%{version}
%{__tar} xzvf $RPM_SOURCE_DIR/%{name}-%{version}.tar.gz
%setup -T -D

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT

make pure_install PERL_INSTALL_ROOT=$RPM_BUILD_ROOT

find $RPM_BUILD_ROOT -type f -name .packlist -exec rm -f {} \;
find $RPM_BUILD_ROOT -depth -type d -exec rmdir {} 2>/dev/null \;
rm -rf $RPM_BUILD_ROOT/usr/lib/debug
rm -rf $RPM_BUILD_ROOT/usr/src/debug

%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc Changes README
%doc /usr/share/man/man3/Linux::Futex.3pm.gz
%{perl_archlib}/*

%changelog
* Fri Jan 24 2014 Nick Townsend <nick.townsend@mac.com> - 0.6
- Packaged for the first time for RHEL 6
